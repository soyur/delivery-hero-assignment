// Using ESRI ArcGIS JS API v3.21
require([
  "esri/map",
  "esri/dijit/Search",
  "esri/geometry/Circle",
  "esri/symbols/SimpleFillSymbol",
  "esri/graphic",
  "esri/dijit/HorizontalSlider",
  "esri/units",
  "esri/Color",
  "esri/geometry/Extent",
  "esri/symbols/SimpleLineSymbol",
  "dojo/domReady!"], function(
    Map,
    Search,
    Circle,
    SimpleFillSymbol,
    Graphic,
    HorizontalSlider,
    Units,
    Color,
    Extent,
  SimpleLineSymbol) {
      var map, openStreetMapLayer, search,buffer,horizontalSlider,buffGraphic;
      var gray = true;

      // INIT MAP
      map = new Map("map", {
        center: [-89.924, 30.036],
        zoom: 12,
        logo: false,
        slider: true,
        sliderPosition:"top-right",
        basemap:'osm'
      });


      // If OSM tiles fail to load or timeout, load topo map instead
      map.on('update-start',function(){
        setTimeout(function(){
          if (gray){
            window.stop()
            map.setBasemap('gray')
          }
        },3000)
      })

      // Success if map was able to load tiles.
      map.on('update-end',function(){
        gray = false;
      })

        // INIT ADDRESS SEARCH BAR
         search = new Search({
            map: map,
            showInfoWindowOnSelect: false,
            enableInfoWindow:false,
            autoNavigate: false,

         }, "search");
         search.startup();

         // EVENT - SELECT SEARCH RESULT
         search.on('select-result',function(){
           buffer(search.highlightGraphic.geometry,5)
           $('.hidden').fadeIn()
         })

          // EVENT - CLOSE SEARCH RESULT
         $('.searchClose').click(function(){
           buffGraphic.hide();
           $('.hidden').fadeOut();
         })

      // INIT SLIDER
      horizontalSlider = new HorizontalSlider({
        labels: [1,10],
        value:50,
        minimum:1
        // showButtons:true
      }, "slider");
      horizontalSlider.startup();

      // EVENT - SLIDE SLIDER
      horizontalSlider.on('change',function(){
          buffer(search.highlightGraphic.geometry,horizontalSlider.value/10)
      })

      // Generate Circle buffer
        // arg1: point geometry
        // arg2: radius in miles
      function buffer(point,rad){
        var buffer = new Circle(point,{radius:rad,radiusUnit:Units.MILES})
        var symbol = new SimpleFillSymbol(
            SimpleLineSymbol.STYLE_SOLID,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
              new Color([0, 0, 0, 1]), 1),
            new Color([255, 0, 0, 0.1]));
        if(buffGraphic){
          buffGraphic.setGeometry(buffer);
          buffGraphic.show();
        } else{
           buffGraphic = new Graphic(buffer, symbol);
        }

        map.graphics.add(buffGraphic); // Add circle graphic to map

        var offset = 1000 // extent offset

        // Generate new map extent from circle radius plus offset
        var extent = new Extent(buffGraphic._extent.xmin - offset, buffGraphic._extent.ymin - offset, buffGraphic._extent.xmax + offset, buffGraphic._extent.ymax + offset, buffGraphic._extent.spatialReference)

        // Set new map extent
        map.setExtent(extent)

        // Build geoJson output in modal
        gSON(buffGraphic.geometry)

        // Update slider radius text
        showBufferDistance(rad)
      } // end buffer function


      // parse ArcGIS JSON, convert it to GeoJSON
      function gSON(circle){
        // convert ArcGIS JSON object to geoJSON
        var geojsonPoint = Terraformer.ArcGIS.parse(circle);
        // sent geoJson string to Modal
        $('#geoJSON').empty().text(JSON.stringify(geojsonPoint,null,2))
      }

      // Update slider radius text
      function showBufferDistance(miles){
        $('#buff').remove()
        $('.dijitSliderImageHandle').after('<div id="buff">'+miles.toFixed(2)+' Miles</div>')
        $('#buff').css({
          position:"absolute",
          marginLeft:'20px'
        })
      }

}); //end require
